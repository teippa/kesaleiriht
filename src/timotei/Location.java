/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

/**
 *
 * @author n1602
 */
public class Location {
    private final String code;
    private final String city;
    private final String address;
    private final String availabilityStr;
    private final String postoffice;
    private final String lat;
    private final String lng;
    
    public Location(String newCode, String newCity, String newAddress, String newAvailabilityStr, String newPostoffice, String newLat, String newLng) {
        code = newCode;
        city = newCity;
        address = newAddress;
        availabilityStr = newAvailabilityStr;
        postoffice = newPostoffice;
        lat = newLat;
        lng = newLng;
    }
    
    
    @Override
    public String toString() {
        return postoffice;
    }
    
    public String getCity() {
        return city;
    }
    public String getAddress() {
        return address + ", " + code + " " + city;
    }
    public String getOther() {
        return (postoffice + ": " + availabilityStr);
    }
    
    public String getLng() {
        return lng;
    }
    public String getLat() {
        return lat;
    }
}
